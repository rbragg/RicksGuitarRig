RicksGuitarRig
==============

My Guitar FX/ Puredata Rig.

  DO NOT USE THIS FOR PRODUCTION.  THIS IS BASED ON DEPRECIATED DEPENDENCIES.
  THIS IS FOR ARCHIVAL PURPOSE.


DEPENDENCIES

  Pure Data Extended: http://puredata.info/downloads/pd-extended
  
  Jack audio server http://www.jackaudio.org/

SYSTEM REQUIREMENTS

  Linux box:
    
    At least 2GB Ram (more will allow you to increase the size of the looper and players)
    
    It is prefered to use a Real Time Kernel.
    
    Otherwise, a really thin system is fine. (I currently use a T41 with CrunchBang)
    
    A decent USB 2 channel audio interface

  See the Schematic and docs for more hardware and other details.

COPYRIGHT AND LICENCE

This work is licensed under a Creative Commons Attribution 4.0 International License.
https://creativecommons.org/licenses/by/4.0/


